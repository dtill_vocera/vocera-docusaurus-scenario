module.exports = {
  title: 'Vocera Documentation Portal',
  tagline: 'Testing the new Vocera theme',
  url: 'https://pubs.vocera.com',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  favicon: 'img/favicon.ico',
  organizationName: 'Vocera', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.
  themeConfig: {
//    algolia: {
//      apiKey: 'YOUR_API_KEY',
//      indexName: 'YOUR_INDEX_NAME',
//    },
    navbar: {
      title: 'Vocera Documentation Portal',
      logo: {
        alt: 'Vocera logo',
        src: 'img/vocera_greenswoop_logo.png',
      },
      items: [],
    },
    footer: {
      // style: 'dark',
      links: [],
      copyright: `Copyright © 2002-${new Date().getFullYear()} Vocera Communications, Inc. All rights reserved.`,
      logo: {
        alt: 'Vocera logo',
        src: 'img/vocera_mini_logo.png',
      },
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          routeBasePath: '/',
          sidebarPath: require.resolve('./sidebars.js'),
          // In order to display a link to edit your documents, please change this value.
          // Refer to Docusaurus v2 documentation for more info.
          // editUrl: 'http://easydita.com',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
